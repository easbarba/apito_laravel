# Apito-Laravel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Apito-Laravel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Apito-Laravel. If not, see <https://www.gnu.org/licenses/>.

FROM docker.io/library/php:8.2-fpm as base
RUN apt-get update && apt-get install -y libpq-dev zip unzip git \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

FROM base as final
MAINTAINER EAS Barbosa <easbarba@outlook.com>
COPY --from=docker.io/library/composer:2 /usr/bin/composer /usr/local/bin/composer
ENV ROOT=/app
WORKDIR $ROOT
COPY . .
RUN composer validate --strict --no-interaction
RUN composer install --no-progress --no-interaction
ENV PATH=$ROOT/vendor/bin:$PATH
CMD ["./bin/start"]
