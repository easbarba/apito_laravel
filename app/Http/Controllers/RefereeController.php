<?php

declare(strict_types=1);

/*
* Apito-Laravel is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Apito-Laravel is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Apito-Laravel. If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use App\Http\Requests\RefereeRequest;
use App\Http\Resources\RefereeResource;
use App\Models\Referee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RefereeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        return response()->json(
            ['data' => RefereeResource::collection(Referee::all())],
            Response::HTTP_OK
        )
            ->header('Allow', Request::METHOD_GET);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RefereeRequest $request): JsonResponse
    {
    }

    /**
     * Display the specified resource.
     */
    public function show(Referee $referee): JsonResponse
    {
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RefereeRequest $request, Referee $referee): JsonResponse
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Referee $referee): JsonResponse
    {
    }
}
